import Sequelize from 'sequelize';

export const sequelize = new Sequelize(
    'fileschat',
    'admin',
    'admin',
    {
        host: '35.226.59.139',
        dialect: 'postgres',
        pool: {
            max: 5,
            min: 0,
            require: 30000,
            idle: 10000
        },
        logging: false
    }
);