import app from './app';
import { appendFileSync } from 'fs';

export async function main() {
    await app.listen(3000);
    console.log('server on port 3000');
};

main();