import { Router } from 'express';
const router = Router();

import {
    getEstructuras,
    getEstructurasRootByUsuario,
    getEstructurasByEstructura,
    newFolder, deleteFolder
} from '../controllers/estructura.controller';

// /api/estructuras/
router.get('/', getEstructuras);

// /api/estructuras/:estructuraid
router.get('/:estructuraid', getEstructurasByEstructura);

// /api/estructuras/usuario/:usuarioid
router.get('/usuario/:usuarioid', getEstructurasRootByUsuario);

router.post('/create', newFolder);

router.delete('/:id', deleteFolder);


export default router;