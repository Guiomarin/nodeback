import { Router } from 'express';
const router = Router();

import { postPermisos, getPermisos } from '../controllers/permiso.controller';

// /api/permisos/
router.post('/', postPermisos);
router.get('/', getPermisos);




export default router;