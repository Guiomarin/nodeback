import { Router } from 'express';
const router = Router();

import {
    postDocumento,
    getDocumentos,
    getDocumentosByUsuario,
    deleteDocumento,
    download, sharedFile
} from '../controllers/documento.controller';

// /api/documentos/
router.post('/', postDocumento);
router.get('/', getDocumentos);

// /api/documentos/:id
router.delete('/:id', deleteDocumento);


// /api/documentos/usuario/:usuarioid
router.get('/usuario/:usuarioid', getDocumentosByUsuario);

router.get('/download/:id', download);
router.post('/shared', sharedFile);


export default router;