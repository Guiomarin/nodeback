import { Router } from 'express';
const router = Router();

import { getUsuarios, postUsuario, putUsuario, deleteUsuario } from '../controllers/usuario.controller';

// /api/usuarios/
router.get('/', getUsuarios);
router.post('/', postUsuario);

// /api/usuarios/:id
router.put('/:id', putUsuario);
router.delete('/:id', deleteUsuario);

export default router;