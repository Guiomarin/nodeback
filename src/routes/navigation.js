import {Router} from 'express';
import {getFilesandFolders} from '../controllers/navigation.controller';

const router = Router();
router.post('/', getFilesandFolders);
export default router;