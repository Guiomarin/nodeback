import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

import Acceso from './Acceso';

const Permiso = sequelize.define('permisos', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    concepto: {
        type: Sequelize.STRING(45)
    }
}, {
    timestamps: false
});

Permiso.hasMany(Acceso, { foreingKey: 'permisoid', sourceKey: 'id'});
Acceso.belongsTo(Permiso, { foreingKey: 'permisoid', sourceKey: 'id'});

export default Permiso;