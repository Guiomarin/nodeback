import Sequelize from 'sequelize';
import {sequelize} from '../database/database';

const Acceso = sequelize.define('accesos', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    documentoid: {
        type: Sequelize.INTEGER
    },
    usuarioid: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false
});

export default Acceso;