import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

import Acceso from './Acceso';
import Documento from './Documento';

const Estructura = sequelize.define('estructuras', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement : true
    },
    nombre: {
        type: Sequelize.STRING(200)
    },
    estructuraid: {
        type: Sequelize.INTEGER
    },
    usuarioid: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false
});

Estructura.hasMany(Estructura, { foreingKey: 'estructuraid', sourceKey: 'id'});
Estructura.belongsTo(Estructura, { foreingKey: 'estructuraid', sourceKey: 'id'});

Estructura.hasMany(Documento, { foreingKey: 'estructuraid', sourceKey: 'id'});
Documento.belongsTo(Estructura, { foreingKey: 'estructuraid', sourceKey: 'id'});

Estructura.hasMany(Acceso, { foreingKey: 'estructuraid', sourceKey: 'id'});
Acceso.belongsTo(Estructura, { foreingKey: 'estructuraid', sourceKey: 'id'});

export default Estructura;