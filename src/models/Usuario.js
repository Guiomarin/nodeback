import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

import Estructura from './Estructura';
import Acceso from './Acceso';
import Documento from './Documento';

const Usuario = sequelize.define('usuarios', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    nombre: {
        type: Sequelize.STRING(45)
    },
    uid: {
        type: Sequelize.STRING(50)
    }
}, {
    timestamps: false
});

Usuario.hasMany(Estructura, { foreingKey: 'usuarioid', sourceKey: 'id'});
Estructura.belongsTo(Usuario, { foreingKey: 'usuarioid', sourceKey: 'id'});

Usuario.hasMany(Acceso, { foreingKey: 'usuarioid', sourceKey: 'id'});
Acceso.belongsTo(Usuario, { foreingKey: 'usuarioid', sourceKey: 'id'});

Usuario.hasMany(Documento, { foreingKey: 'usuarioid', sourceKey: 'id'});
Documento.belongsTo(Usuario, { foreingKey: 'usuarioid', sourceKey: 'id'});

export default Usuario;