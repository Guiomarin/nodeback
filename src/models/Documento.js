import Sequelize from 'sequelize';
import { sequelize } from '../database/database';

import Acceso from './Acceso';

const Documento = sequelize.define('documentos', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement : true
    },    
    nombre: {
        type: Sequelize.STRING(200)
    },    
    extension: {
        type: Sequelize.STRING(45)
    },
    tamaño: {
        type: Sequelize.INTEGER
    },
    versio: {
        type: Sequelize.STRING(45)
    },
    creacion: {
        type: Sequelize.DATE
    },
    modificado: {
        type: Sequelize.DATE
    },    
    estructuraid: {
        type: Sequelize.INTEGER
    },
    usuarioid: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false
});

Documento.hasMany(Acceso, { foreingKey: 'documentoid', sourceKey: 'id'});
Acceso.belongsTo(Documento, { foreingKey: 'documentoid', sourceKey: 'id'});

export default Documento;