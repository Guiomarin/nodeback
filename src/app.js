import express, {json} from 'express';
import morgan from 'morgan';
//Importing routes
import usuarioRoutes from './routes/usuarios';
import permisoRoutes from './routes/permisos';
import estructuraRoutes from './routes/estructuras';
import documentoRoutes from './routes/documentos';
import navigationRouter from './routes/navigation';

//initialization
const app = express();
const upload = require('express-fileupload');
const path = require('path');



//middlewares
app.use(morgan('dev'));
app.use(json());
app.use(upload());




//routes
app.use('/api/usuarios', usuarioRoutes);
app.use('/api/permisos', permisoRoutes);
app.use('/api/estructuras', estructuraRoutes);
app.use('/api/estructuras/usuario', estructuraRoutes);
app.use('/api/documentos', documentoRoutes);
app.use('/api/documentos/descargar', documentoRoutes);
app.use('/api/documentos/usuario', estructuraRoutes);
app.use('/api/navigation', navigationRouter);

export default app;