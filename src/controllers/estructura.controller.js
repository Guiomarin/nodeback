import Estructura from '../models/Estructura';
import {getUserfromUID} from "./usuario.controller";

export async function getEstructuras(req, res) {
    const estructura = await Estructura.findAll({
        attributes: ['id', 'nombre', 'estructuraid', 'usuarioid'],
        order: [
            ['id', 'DESC']
        ]
    });
    res.json({estructura});
}

export async function getEstructurasRootByUsuario(req, res) {
    const {usuarioid} = req.params;
    const estructuras = await Estructura.findAll({
        attributes: ['id', 'nombre', 'estructuraid', 'usuarioid'],
        where: {estructuraid: null, usuarioid}
    });
    res.json({estructuras});
}

export async function getEstructurasByEstructura(req, res) {
    const {estructuraid} = req.params;
    const estructuras = await Estructura.findAll({
        attributes: ['id', 'nombre', 'estructuraid', 'usuarioid'],
        where: {estructuraid}
    });
    res.json({estructuras});
}

export async function newFolder(req, res) {
    console.log(req.body);
    const root = req.body.idFolder == "null" ? null : req.body.idFolder;
    var owner = req.body.owner;
    owner = await getUserfromUID(owner);
    console.log(owner.id);
    const name = req.body.name;

    Estructura.create({
        nombre: name,
        estructuraid: root,
        usuarioid: owner.id
    });

    res.json({message: "Carpeta creada correctamente"});
}


export async function deleteFolder(req, res) {
    const {id} = req.params;
    Estructura.destroy({
        where: {
            id
        }
    });

    res.json({message: "Carpeta eliminada correctamente"});
}