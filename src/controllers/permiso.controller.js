import Permiso from '../models/Permiso';

export async function getPermisos(req, res) {
    const permisos = await Permiso.findAll();
    res.json({
        data: permisos
    });
}

export async function postPermisos(req, res) {
    const { concepto } = req.body;
    try {
        let newPermiso = await Permiso.create({
            concepto,
        }, {
            fields: ['concepto']
        });
        if (newPermiso) {
            return res.json({
                message: 'Permiso created successfully',
                data: newPermiso
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        });
    }
};
