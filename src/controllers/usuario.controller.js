import Usuario from '../models/Usuario';

export async function getUsuarios(req, res) {
    const usuarios = await Usuario.findAll();
    res.json({
        data: usuarios
    });
}

export async function postUsuario(req, res) {
    const { idfirebase, nombre} = req.body;
    try {
        let newUsuario = await Usuario.create({
            idfirebase,
            nombre
        }, {
            fields: ['nombre']
        });
        if (newUsuario) {
            return res.json({
                message: 'Usuario created successfully',
                data: newUsuario
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Something goes wrong',
            data: {}
        });
    }
};

export async function putUsuario(req, res) {
    const { idusuario } = req.params;
    const { nombre } = req.body;


    const putUsuario = await Usuario.update({
        nombre
    }, {
        where: {idusuario}
    });
    
    const usuario = await Usuario.findOne({
        attributes: ['id', 'nombre'],
        where: {idusuario}
    });

    res.json({
        message: 'Usuario updated',
        usuario
    })
}

export async function deleteUsuario(req, res) {
    const { id } = req.params;
    await Usuario.destroy({
        where: {
            id
        }
    });
    res.json({message: "Usuario Deleted"});
}

export async function getUserfromUID(uid) {
    const dataUser = await Usuario.findOne({
        attributes: ['id', 'nombre', 'uid'],
        where: {'uid': uid}
    });
    return dataUser;
}