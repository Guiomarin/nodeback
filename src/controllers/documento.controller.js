import Documento from '../models/Documento';
import Usuario from '../models/Usuario';
import Acceso from "../models/Acceso";

export async function postDocumento(req, res) {
    if (Array.isArray(req.files.upfile)) {
        req.files.upfile.forEach(function (upfile) {
            if (upfile) {
                cargarArchivo(upfile, req.body);
                res.json({message: "Archivo (s) cargados correctamente"});
                //res.end();
            } else {
                res.send("No File selected !");
                //res.end();
            }
        });
    } else {
        if (req.files.upfile) {
            cargarArchivo(req.files.upfile, req.body).then((data) => {
                console.log(data);
            });
            res.json({message: "Archivo (s) cargados correctamente"});
            //res.end();
        } else {
            res.send("No File selected !");
            //res.end();
        }
    }

}

//----Funcion para cargar varios archivos en la carpeta uploads del servidor
async function cargarArchivo(upfile, meta) {
    const file = upfile,
        name = file.name,
        type = file.mimetype;

    const uploadpath = '../uploads/' + name;
    await file.mv(uploadpath, function (err) {
        if (err) {
            return {'error': true, 'msg': "File Upload Failed" + name, err};
        } else {
            return {'error': false, 'msg': "File Uploaded" + name};
        }
    });
    const data = await getUser(meta.idUser);
    const userId = data.id;

    await Documento.create({
        nombre: file.name,
        extension: file.name.toString().split('.')[file.name.toString().split('.').length - 1],
        tamaño: file.size,
        versio: '1',
        creacion: new Date(),
        modificado: new Date(),
        estructuraid: (meta.idFolder == "null" ? null : meta.idFolder),
        usuarioid: userId
    });

}


export async function getUser(uid) {
    const dataUser = await Usuario.findOne({
        attributes: ['id', 'nombre', 'uid'],
        where: {'uid': uid}
    });
    return dataUser;
}

//----

export async function getDocumentos(req, res) {
    const documentos = await Documento.findAll({
        attributes: ['id', 'nombre', 'extension', 'tamaño', 'versio', 'creacion', 'modificado', 'estructuraid', 'usuarioid'],
        order: [
            ['id', 'DESC']
        ]
    });
    res.json({documentos});
}

export async function getDocumentosByUsuario(req, res) {
    const {usuarioid} = req.params;
    const documentos = await Documento.findAll({
        attributes: ['id', 'nombre', 'extension', 'tamaño', 'versio', 'creacion', 'modificado', 'estructuraid', 'usuarioid'],
        where: {usuarioid}
    });
    res.json({documentos});
}


export async function deleteDocumento(req, res) {
    const {id} = req.params;
    await Documento.destroy({
        where: {
            id
        }
    });
    res.json({message: "Documento eliminado correctamente"});
}


export async function download(req, res) {
    const {id} = req.params;
    const dataFile = await Documento.findOne({
        attributes: ['id', 'nombre'],
        where: {id}
    });

    //const name = "Bebe Rexha - Last Hurrah.mp3"
    const name = dataFile.nombre;
    const uploadpath = '/home/guio_marin/projects/uploads/' + name;
    res.download(uploadpath);
}

export async function sharedFile(req, res) {
    console.log(req.body);
    req.body.users.toString().split(',').forEach(item =>
    {
        Acceso.create({
            documentoid: req.body.idFile,
            usuarioid: item
        });
    });
    res.json({message: "Elemento compatido a las personas seleccionadas"});
}