import Documento from '../models/Documento';
import Usuario from '../models/Usuario';
import Estructura from "../models/Estructura";
import {Op} from "sequelize";
import {sequelize} from '../database/database';

export async function getFilesandFolders(req, res) {
    var items = [];
    console.log(req.body);
    const root = req.body.idFolder == "null" ? null : req.body.idFolder;
    var owner = req.body.owner;
    owner = await getUser(owner);
    console.log(owner.id);
    const search = req.body.search;

    if (root != null) {
        const rootFolder = await Estructura.findOne({
            attributes: ['id', 'nombre', 'usuarioid', 'estructuraid'],
            where: {'id': root}
        });

        items.push({
            id: rootFolder == null ? null : rootFolder.estructuraid,
            name: "Retornar a la carpeta anterior",
            type: "back",
            iconClass: "pink white--text",
            icon: "arrow_back",
            userId: owner.id
        });
    } else
        items.push({
            id: "0",
            name: "Compartidos Conmigo",
            type: "folder",
            iconClass: "teal white--text",
            icon: "folder_shared",
            userId: owner.id
        });


    if (root == "0") {
        const x = await sequelize.query("Select * from documentos where nombre like '%" + search + "%' and id in (select documentoid from accesos where usuarioid = " + owner.id + ") ", {type: sequelize.QueryTypes.SELECT})
            .then(async docsShared => {
                for (const item of docsShared) {
                    const prop = await getUserId(item.usuarioid);
                    items.push({
                        id: item.id,
                        name: item.nombre,
                        type: "file",
                        iconClass: "purple white--text",
                        icon: "mdi-file",
                        size: item.tamaño,
                        version: item.versio,
                        dateCreate: item.creacion,
                        dateAccess: item.modificado,
                        owner: prop.nombre,
                        userId: item.usuarioid,
                        isOwner: false
                    });
                }
            });


    } else {
        const estructuras = await Estructura.findAll({
            attributes: ['id', 'nombre', 'usuarioid'],
            where: {
                estructuraid: root, usuarioid: owner.id, nombre: {
                    [Op.substring]: search
                }
            }
        });

        for (const item of estructuras) {
            const prop = await getUserId(item.usuarioid);
            items.push({
                id: item.id,
                name: item.nombre,
                type: "folder",
                iconClass: "grey white--text",
                icon: "mdi-folder",
                owner: prop.nombre,
                userId: item.usuarioid,
                isOwner: true
            });
        }


        const documentos = await Documento.findAll({
            attributes: ['id', 'nombre', 'extension', 'tamaño', 'versio', 'creacion', 'modificado', 'estructuraid', 'usuarioid'],
            where: {
                estructuraid: root, usuarioid: owner.id, nombre: {
                    [Op.substring]: search
                }
            }
        });

        for (const item of documentos) {
            const prop = await getUserId(item.usuarioid);
            items.push({
                id: item.id,
                name: item.nombre,
                type: "file",
                iconClass: "amber white--text",
                icon: "mdi-file",
                size: item.tamaño,
                version: item.versio,
                dateCreate: item.creacion,
                dateAccess: item.modificado,
                owner: prop.nombre,
                userId: item.usuarioid,
                isOwner: true
            });
        }
    }

    res.json(items);
}


export async function getUser(uid) {
    const dataUser = await Usuario.findOne({
        attributes: ['id', 'nombre', 'uid'],
        where: {'uid': uid}
    });
    return dataUser;
}

export async function getUserId(id) {
    const dataUser = await Usuario.findOne({
        attributes: ['id', 'nombre', 'uid'],
        where: {'id': id}
    });
    return dataUser;
}
